package com.azedev.rxfit.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.azedev.rxfit.R;


/**
 * Class type DialogBubbleLayout.
 */
public class DialogBubbleLayout extends ViewGroup {
    public static final String TAG = DialogBubbleLayout.class.getName();

    private static final int LEFT = 0;
    private static final int TOP = 1;
    private static final int RIGHT = 2;
    private static final int BOTTOM = 3;

    private static final int TYPE_SYMMETRICAL = 0;
    private static final int TYPE_ANGLED_IN = -1;
    private static final int TYPE_ANGLED_OUT = 1;

    private Paint paintFill;
    private Paint paintShadow;
    private Integer shadowOffset;
    private Integer cornerRadius;
    private Integer arrowDirection;
    private Integer arrowType;
    private Integer arrowWidth;
    private Integer arrowLength;
    private Integer arrowPositionAbsolute;
    private Float arrowPositionPercent;


    /**
     * Instantiates a new Dialog bubble layout.
     *
     * @param context the context
     */
    public DialogBubbleLayout(Context context) {
        super(context);
    }

    /**
     * Instantiates a new Dialog bubble layout.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public DialogBubbleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Instantiates a new Dialog bubble layout.
     *
     * @param context      the context
     * @param attrs        the attrs
     * @param defStyleAttr the def style attr
     */
    public DialogBubbleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        setWillNotDraw(false);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DialogBubbleLayout, 0, 0);

        setFillPaint(a.getColor(R.styleable.DialogBubbleLayout_bubble_color, 0));
        setShadowPaint(a.getColor(R.styleable.DialogBubbleLayout_shadow_color, 0));
        setShadowOffset(a.getDimensionPixelSize(R.styleable.DialogBubbleLayout_shadow_offset, 0));
        setCornerRadius(a.getDimensionPixelSize(R.styleable.DialogBubbleLayout_corner_radius, 0));
        setArrowDirection(a.getInt(R.styleable.DialogBubbleLayout_arrow_direction, 0));
        setArrowType(a.getInt(R.styleable.DialogBubbleLayout_arrow_type, 0));
        setArrowWidth(a.getDimensionPixelSize(R.styleable.DialogBubbleLayout_arrow_width, 0));
        setArrowLength(a.getDimensionPixelSize(R.styleable.DialogBubbleLayout_arrow_length, 0));
        setArrowPositionPercent(a.getFloat(R.styleable.DialogBubbleLayout_arrow_position_percent, 0));
        setArrowPositionAbsolute(a.getDimensionPixelSize(R.styleable.DialogBubbleLayout_arrow_position_absolute, 0));

        a.recycle();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRoundRect(getBubbleRect(0), cornerRadius, cornerRadius, paintShadow);
        canvas.drawPath(getArrowPath(0), paintShadow);

        canvas.drawRoundRect(getBubbleRect(-shadowOffset), cornerRadius, cornerRadius, paintFill);
        canvas.drawPath(getArrowPath(-shadowOffset), paintFill);
    }

    private RectF getBubbleRect(int shadowSize) {
        RectF rect = new RectF();
        switch(arrowDirection) {
            case LEFT:
                rect.set(arrowLength, 0, getWidth(), getHeight() + shadowSize);
                break;
            case TOP:
                rect.set(0, arrowLength, getWidth(), getHeight() + shadowSize);
                break;
            case RIGHT:
                rect.set(0, 0, getWidth() - arrowLength, getHeight() + shadowSize);
                break;
            case BOTTOM:
                rect.set(0, 0, getWidth(), getHeight() - arrowLength + shadowSize);
                break;
        }
        return rect;
    }

    private Point getArrowOriginPoint(int shadowSize) {
        final int width = getWidth() - (arrowWidth / 2) - (cornerRadius * 3);
        final int height = getHeight() - (arrowWidth / 2) - (cornerRadius * 3);
        Point originPoint = new Point();
        switch(arrowDirection) {
            case TOP:
                originPoint.set(getRoundCoord(width), arrowLength);
                break;
            case RIGHT:
                originPoint.set(getWidth()-arrowLength, getRoundCoord(height) + shadowSize);
                break;
            case BOTTOM:
                originPoint.set(getRoundCoord(width), getHeight() - arrowLength + shadowSize);
                break;
            case LEFT:
                originPoint.set(arrowLength, getRoundCoord(height) + shadowSize);
                break;
        }
        return originPoint;
    }

    private int getRoundCoord(int dimen) {
        return Math.round(dimen * arrowPositionPercent) + (cornerRadius * 2);
    }

    private Path getArrowPath(int shadowSize) {
        Path path = new Path();
        Point p1 = getArrowOriginPoint(shadowSize), p2 = null, p3 = null, p4 = null;

        switch (arrowDirection){
            case LEFT:
                p2 = new Point(p1.x, p1.y + (arrowWidth / 2));
                p3 = new Point(p1.x - arrowLength, p1.y + getArrowAngle());
                p4 = new Point(p1.x, p1.y - (arrowWidth / 2));
                break;
            case TOP:
                p2 = new Point(p1.x + (arrowWidth / 2), p1.y);
                p3 = new Point(p1.x + getArrowAngle(), p1.y - arrowLength);
                p4 = new Point(p1.x - (arrowWidth / 2), p1.y);
                break;
            case RIGHT:
                p2 = new Point(p1.x, p1.y + (arrowWidth / 2));
                p3 = new Point(p1.x + arrowLength, p1.y + getArrowAngle());
                p4 = new Point(p1.x, p1.y - (arrowWidth / 2));
                break;
            case BOTTOM:
                p2 = new Point(p1.x + (arrowWidth / 2), p1.y);
                p3 = new Point(p1.x + getArrowAngle(), p1.y + arrowLength);
                p4 = new Point(p1.x - (arrowWidth / 2), p1.y);
                break;
        }

        path.moveTo(p1.x, p1.y);
        path.lineTo(p2.x, p2.y);
        path.lineTo(p3.x, p3.y);
        path.lineTo(p4.x, p4.y);

        return path;
    }

    private int getArrowAngle() {

//        final int width = getWidth() - (arrowWidth / 2) - (cornerRadius * 3);
//        final int height = getHeight() - (arrowWidth / 2) - (cornerRadius * 3);

        if (arrowType != TYPE_SYMMETRICAL) {
            if (arrowPositionPercent >= 0.5)
                return (arrowWidth / 2) * (arrowType == TYPE_ANGLED_IN ? -1 : 1);
            else
                return (arrowWidth / 2) * (arrowType == TYPE_ANGLED_OUT ? -1 : 1);
        } else
            return 0;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (getChildCount() != 0) {
            int itemWidth = (r - l) / getChildCount();
            for(int i = 0; i < getChildCount(); i++){
                View v = getChildAt(i);
                switch (arrowDirection){
                    case LEFT:
                        v.layout((itemWidth * i) + arrowLength, t, (i + 1) * itemWidth, b);
                        v.layout((itemWidth * i) + arrowLength, 0, (i + 1) * itemWidth, b - t);
                        break;
                    case TOP:
                        v.layout(itemWidth * i, t + arrowLength, (i + 1) * itemWidth, b);
                        v.layout(itemWidth * i, arrowLength, (i + 1) * itemWidth, b - t);
                        break;
                    case RIGHT:
                        v.layout(itemWidth * i, t, ((i + 1) * itemWidth) - arrowLength, b);
                        v.layout(itemWidth * i, 0, ((i + 1) * itemWidth) - arrowLength, b - t);
                        break;
                    case BOTTOM:
                        v.layout(itemWidth * i, t, (i + 1) * itemWidth, b - arrowLength);
                        v.layout(itemWidth * i, 0, (i + 1) * itemWidth, (b - t) - arrowLength);
                        break;
                }
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        int totalWidth = 0;
        int totalHeight = 0;
        int count = getChildCount();
        if (count != 0) {
            for (int i = 0; i < count; i++) {
                final View child = getChildAt(i);
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
                totalWidth += child.getMeasuredWidth();
                if (child.getMeasuredHeight() > totalHeight) {
                    totalHeight = child.getMeasuredHeight();
                }
            }
            if (arrowDirection == TOP || arrowDirection == BOTTOM)
                setMeasuredDimension(totalWidth, totalHeight + arrowLength + shadowOffset);
            else
                setMeasuredDimension(totalWidth + arrowLength, totalHeight + shadowOffset);
        } else
            setMeasuredDimension(widthMeasureSpec, heightMeasureSpec + shadowOffset);
    }

    /**
     * Sets corner radius.
     *
     * @param cornerRadius the corner radius
     */
    public void setCornerRadius(Integer cornerRadius) {
        this.cornerRadius = cornerRadius;
        invalidate();
    }


    /**
     * Sets arrow type.
     *
     * @param arrowType the arrow type
     */
    public void setArrowType(Integer arrowType) {
        this.arrowType = arrowType;
        invalidate();
    }

    /**
     * Sets arrow direction.
     *
     * @param arrowDirection the arrow direction
     */
    public void setArrowDirection(Integer arrowDirection) {
        this.arrowDirection = arrowDirection;
        invalidate();
    }

    /**
     * Sets arrow width.
     *
     * @param arrowWidth the arrow width
     */
    public void setArrowWidth(Integer arrowWidth) {
        this.arrowWidth = arrowWidth;
        invalidate();
    }

    /**
     * Sets shadow offset.
     *
     * @param shadowOffset the shadow offset
     */
    public void setShadowOffset(Integer shadowOffset) {
        this.shadowOffset = shadowOffset;
        invalidate();
    }

    /**
     * Sets arrow position absolute.
     *
     * @param arrowPositionAbsolute the arrow position absolute
     */
    public void setArrowPositionAbsolute(Integer arrowPositionAbsolute) {
        this.arrowPositionAbsolute = arrowPositionAbsolute;
        invalidate();
    }

    /**
     * Sets arrow position percent.
     *
     * @param arrowPositionPercent the arrow position percent
     */
    public void setArrowPositionPercent(Float arrowPositionPercent) {
        if (arrowPositionPercent <= 1f)
            this.arrowPositionPercent = arrowPositionPercent;
        else
            this.arrowPositionPercent = 0f;
        invalidate();
    }

    /**
     * Sets arrow length.
     *
     * @param arrowLength the arrow length
     */
    public void setArrowLength(Integer arrowLength) {
        if (arrowLength != 0)
            this.arrowLength = arrowLength;
        else
            this.arrowLength = arrowWidth;
        invalidate();
    }

    /**
     * Sets shadow paint.
     *
     * @param shadowColor the shadow color
     */
    public void setShadowPaint(Integer shadowColor) {
        this.paintShadow = new Paint();
        this.paintShadow.setStyle(Paint.Style.FILL);
        this.paintShadow.setAntiAlias(true);

        if (shadowColor != 0)
            this.paintShadow.setColor(shadowColor);
        else
            this.paintShadow.setColor(Color.BLACK);
        invalidate();
    }

    /**
     * Sets fill paint.
     *
     * @param bubbleColor the bubble color
     */
    public void setFillPaint(Integer bubbleColor) {
        this.paintFill = new Paint();
        this.paintFill.setStyle(Paint.Style.FILL);
        this.paintFill.setAntiAlias(true);

        if (bubbleColor != 0)
            this.paintFill.setColor(bubbleColor);
        else
            this.paintFill.setColor(Color.WHITE);
        invalidate();
    }

}
