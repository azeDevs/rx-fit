package com.azedev.rxfit.eventbus;

import com.google.firebase.auth.FirebaseAuth;


public class LoginEvent {

    public enum EventDef {
        DEFAULT,
        SIGN_IN,
        SIGN_OUT
    }

    public final EventDef eventDef;

    public LoginEvent(EventDef eventDef) {
        this.eventDef = getEventDef(eventDef);
    }

    private EventDef getEventDef(EventDef eventDef) {
        if (eventDef == EventDef.DEFAULT) {
            if (FirebaseAuth.getInstance().getCurrentUser() == null)
                return EventDef.SIGN_IN;
            else
                return EventDef.SIGN_OUT;
        }
        return eventDef;
    }

}
