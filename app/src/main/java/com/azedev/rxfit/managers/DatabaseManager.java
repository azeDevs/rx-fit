package com.azedev.rxfit.managers;

import android.util.Log;

import com.azedev.rxfit.models.FoodItem;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;


public class DatabaseManager {
	public static final String TAG = DatabaseManager.class.getName();

	private static DatabaseManager instance;
	private FirebaseDatabase firebaseDatabase = null;
	private ValueEventListener databaseConnectionListener;

	private DatabaseReference foodItemsDatabaseReference = null;
	private Observer<List<FoodItem>> foodItemsDBSubscriber;
	private List<FoodItem> foodItemsModelList;
	private List<String> keys;


	private DatabaseManager() {
		setupDatabaseConnectionListener();
	}


	public static DatabaseManager getInstance() {
		if (instance == null) instance = new DatabaseManager();
		return instance;
	}

	public void setupDatabaseConnectionListener() {
		databaseConnectionListener = getFoodItemsDatabaseReference().getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
//				boolean connected = (Boolean) dataSnapshot.getValue();
//				if (connected) firebaseStatus.setVisibility(View.VISIBLE);
//				else firebaseStatus.setVisibility(View.INVISIBLE);
			}
			@Override
			public void onCancelled(DatabaseError databaseError) {
				Log.e(TAG, "ValueEventListener in databaseConnectionListener has encountered an error: " + databaseError.toString());
			}
		});
	}

	public void removeDatabaseConnectionListener() {
		getFoodItemsDatabaseReference().getRoot().child(".info/connected").removeEventListener(databaseConnectionListener);
	}

	public DatabaseReference getFoodItemsDatabaseReference() {
		if (foodItemsDatabaseReference == null) {
			foodItemsDatabaseReference = getFirebaseDatabaseInstance().getReference("food");
			foodItemsDatabaseReference.keepSynced(true);
		}
		return foodItemsDatabaseReference;
	}

	public void setFoodItemsSubscriber(Observer<List<FoodItem>> databaseSubscriber) {
		this.foodItemsDBSubscriber = databaseSubscriber;
		this.foodItemsModelList = new ArrayList<>();
		this.keys = new ArrayList<>();
		setFoodItemsChangeListener();
	}

	private FirebaseDatabase getFirebaseDatabaseInstance() {
		if (firebaseDatabase == null) {
			firebaseDatabase = FirebaseDatabase.getInstance();
			firebaseDatabase.setPersistenceEnabled(true);
		}
		return firebaseDatabase;
	}

	private void setFoodItemsChangeListener() {
		// Look for all child events. We will then map them to our own internal ArrayList, which backs ListView
		foodItemsDatabaseReference.addChildEventListener(new ChildEventListener() {
			@Override
			public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
				FoodItem model = dataSnapshot.getValue(FoodItem.class);
				String key = dataSnapshot.getKey();
				// Insert into the correct location, based on previousChildName
				if (previousChildName == null) {
					foodItemsModelList.add(0, model);
					keys.add(0, key);
				} else {
					int previousIndex = keys.indexOf(previousChildName);
					int nextIndex = previousIndex + 1;
					if (nextIndex == foodItemsModelList.size()) {
						foodItemsModelList.add(model);
						keys.add(key);
					} else {
						foodItemsModelList.add(nextIndex, model);
						keys.add(nextIndex, key);
					}
				}

				Observable.just(foodItemsModelList).subscribe(foodItemsDBSubscriber);
			}

			@Override
			public void onChildChanged(DataSnapshot dataSnapshot, String s) {
				// One of the models in foodItemsModelList changed. Replace it in our list and name mapping
				String key = dataSnapshot.getKey();
				FoodItem newModel = dataSnapshot.getValue(FoodItem.class);
				int index = keys.indexOf(key);
				foodItemsModelList.set(index, newModel);

				Observable.just(foodItemsModelList).subscribe(foodItemsDBSubscriber);
			}

			@Override
			public void onChildRemoved(DataSnapshot dataSnapshot) {
				// A model was removed from the list. Remove it from our list and the name mapping
				String key = dataSnapshot.getKey();
				int index = keys.indexOf(key);

				keys.remove(index);
				foodItemsModelList.remove(index);

				Observable.just(foodItemsModelList).subscribe(foodItemsDBSubscriber);
			}

			@Override
			public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
				// A model changed position in the list. Update our list accordingly
				String key = dataSnapshot.getKey();
				FoodItem newModel = dataSnapshot.getValue(FoodItem.class);
				int index = keys.indexOf(key);
				foodItemsModelList.remove(index);
				keys.remove(index);
				if (previousChildName == null) {
					foodItemsModelList.add(0, newModel);
					keys.add(0, key);
				} else {
					int previousIndex = keys.indexOf(previousChildName);
					int nextIndex = previousIndex + 1;
					if (nextIndex == foodItemsModelList.size()) {
						foodItemsModelList.add(newModel);
						keys.add(key);
					} else {
						foodItemsModelList.add(nextIndex, newModel);
						keys.add(nextIndex, key);
					}
				}

				Observable.just(foodItemsModelList).subscribe(foodItemsDBSubscriber);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
				Log.e(TAG, "Listen was cancelled, no more updates will occur");
			}

		});
	}

}
