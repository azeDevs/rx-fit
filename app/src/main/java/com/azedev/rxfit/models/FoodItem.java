package com.azedev.rxfit.models;

public class FoodItem extends MacroStatsItem {

	public double count;
	private UnitsOfMeasurement unitsOfMeasurement;

	public FoodItem(String type, String name, UnitsOfMeasurement unitsOfMeasurement, MacroStats macroStats, String notes) {
		this.setType(type);
		this.setName(name);
		this.setNotes(notes);
		this.setMacros(macroStats);
		this.unitsOfMeasurement = unitsOfMeasurement;
		this.count = 1;
	}

	public FoodItem(FoodItem foodItem) {
		this.setType(foodItem.getType());
		this.setName(foodItem.getName());
		this.setNotes(foodItem.getNotes());
		this.setMacros(foodItem.getMacros());
		this.unitsOfMeasurement = foodItem.getUnitsOfMeasurement();
		this.count = foodItem.count;
	}

	public FoodItem() {
	}

	public UnitsOfMeasurement getUnitsOfMeasurement() {
		return unitsOfMeasurement;
	}

	public void setUnitsOfMeasurement(UnitsOfMeasurement unitsOfMeasurement) {
		this.unitsOfMeasurement = unitsOfMeasurement;
	}

	public MacroStats getStackOfMacros() {
		this.voidMacros();
		this.setCalories(this.getCalories() * count);
		this.setProtein(this.getProtein() * count);
		this.setCarbs(this.getCarbs() * count);
		this.setFat(this.getFat() * count);

		return this.getMacros();
	}

	@Override
	public String toString() {
		return
				"FoodsItem{" +
						"notes = '" + this.getNotes() + '\'' +
						",carbs = '" + this.getCarbs() + '\'' +
						",protein = '" + this.getProtein() + '\'' +
						",name = '" + this.getName() + '\'' +
						",standardUnits = '" + unitsOfMeasurement.getStandardUnits() + '\'' +
						",standardMeasurement = '" + unitsOfMeasurement.getStandardMeasurement() + '\'' +
						",fat = '" + this.getFat() + '\'' +
						",calories = '" + this.getCalories() + '\'' +
						",type = '" + this.getType() + '\'' +
						",abstractMeasurement = '" + unitsOfMeasurement.getAbstractMeasurement() + '\'' +
						",abstractUnits = '" + unitsOfMeasurement.getAbstractUnits() + '\'' +
						"}";
	}

}
