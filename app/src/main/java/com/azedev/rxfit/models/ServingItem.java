package com.azedev.rxfit.models;


import java.util.ArrayList;


public class ServingItem extends MacroStatsItem {

    private ArrayList<FoodItem> serving;

    public ServingItem() {
        super.setName("New Serving");
        this.serving = new ArrayList<>();
    }

    public int getNumberOfUniqueFoodItems() {
        // TODO: USE THIS TO INDICATE THE NUMBER OF ITEMS ADDED IN THE TOPNAV
        return 0;
    }

    public MacroStats getServingMacros() {
        this.voidMacros();
        for (FoodItem foodItemStack : serving) {
            this.addToMacros(foodItemStack.getStackOfMacros());
        }
        return new MacroStats(this.getMacros());
    }

    public void addOneFood(FoodItem incomingFood) {
        for (FoodItem foodItem : serving) {
            if (foodItem.getName().equals(incomingFood.getName())) {
                // +1 an existing stack
                foodItem.count++;
            } else {
                // start a new stack
                serving.add(new FoodItem(incomingFood));
            }
        }
    }

}
