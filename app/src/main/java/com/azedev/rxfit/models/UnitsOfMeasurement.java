package com.azedev.rxfit.models;

public class UnitsOfMeasurement {

	private String standardMeasurement;
	private String abstractMeasurement;
	private Double standardUnits;
	private Double abstractUnits;

	public UnitsOfMeasurement(String standardMeasurement, String abstractMeasurement, Double standardUnits, Double abstractUnits) {
		this.standardMeasurement = standardMeasurement;
		this.abstractMeasurement = abstractMeasurement;
		this.standardUnits = standardUnits;
		this.abstractUnits = abstractUnits;
	}

	public UnitsOfMeasurement() {
		this.standardMeasurement = "";
		this.abstractMeasurement = "";
		this.standardUnits = 0d;
		this.abstractUnits = 0d;
	}

	public String getStandardMeasurement() {
		return standardMeasurement;
	}

	public void setStandardMeasurement(String standardMeasurement) {
		this.standardMeasurement = standardMeasurement;
	}

	public String getAbstractMeasurement() {
		return abstractMeasurement;
	}

	public void setAbstractMeasurement(String abstractMeasurement) {
		this.abstractMeasurement = abstractMeasurement;
	}

	public Double getStandardUnits() {
		return standardUnits;
	}

	public void setStandardUnits(Double standardUnits) {
		this.standardUnits = standardUnits;
	}

	public Double getAbstractUnits() {
		return abstractUnits;
	}

	public void setAbstractUnits(Double abstractUnits) {
		this.abstractUnits = abstractUnits;
	}

	private String formatDecimals(String in, int decimalPlaces) {
		decimalPlaces = decimalPlaces == 0 || decimalPlaces < 0 ? 0 : decimalPlaces + 1;
		return in.substring(0, in.indexOf(".") + decimalPlaces);
	}

	public String getFormattedStandardValue() {
		if (standardMeasurement.length() > 0) {
			if (standardUnits % 1 == 0)
				return (formatDecimals(standardUnits.toString(), 0) + " " + standardMeasurement);
			else return (standardUnits + " " + standardMeasurement);
		}
		return "";
	}

	public String getFormattedAbstractValue() {
		if (abstractMeasurement.length() > 0) {
			if (abstractUnits % 1 == 0)
				return (formatDecimals(abstractUnits.toString(), 0) + " " + abstractMeasurement);
			else return (abstractUnits + " " + abstractMeasurement);
		}
		return "";
	}


	@Override
	public String toString() {
		return
				"UnitsOfMeasurement{" +
						"standardMeasurement = '" + standardMeasurement + '\'' +
						",abstractMeasurement = '" + abstractMeasurement + '\'' +
						",standardUnits = '" + standardUnits + '\'' +
						",abstractUnits = '" + abstractUnits + '\'' +
						"}";
	}

}
