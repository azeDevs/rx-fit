package com.azedev.rxfit.managers;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.azedev.rxfit.R;
import com.azedev.rxfit.eventbus.SwapFragmentEvent;
import com.azedev.rxfit.eventbus.ToastMessageEvent;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.annotations.NonNull;


public class AuthSessionManager implements GoogleApiClient.OnConnectionFailedListener{
    public static final String TAG = AuthSessionManager.class.getName();
    public static final int RC_SIGN_IN = 9001;

    // TODO: Remove all instances of context from AuthSessionManager, if needed, move things to LoginFragment.
    private Activity context;
    private GoogleApiClient googleApiClient;
    private FirebaseAuth firebaseAuth;

    // TODO: Write unit tests to validate all objects are instantiated properly.
    public AuthSessionManager(Activity context) {
        this.context = context;
        this.firebaseAuth = FirebaseAuth.getInstance();
        setupGoogleApiClient();
    }

    public FirebaseUser getConnectedUser() {
        return firebaseAuth.getCurrentUser();
    }

    public void signOut() {
        firebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                status -> EventBus.getDefault().post(new SwapFragmentEvent(SwapFragmentEvent.EventDef.LOGIN)));
    }

    public void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        context.startActivityForResult(signInIntent, AuthSessionManager.RC_SIGN_IN);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: success");
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            Log.d(TAG, "firebaseAuthWithGoogle: " + account.getId());
            AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            firebaseAuth.signInWithCredential(credential).addOnCompleteListener(context, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithCredential: success");
                        EventBus.getDefault().post(new ToastMessageEvent("Authentication succeeded"));
                        EventBus.getDefault().post(new SwapFragmentEvent(SwapFragmentEvent.EventDef.NUTRITION));
                    }
                    else {
                        Log.w(TAG, "signInWithCredential: failure", task.getException());
                        EventBus.getDefault().post(new ToastMessageEvent("Authentication failed"));
                    }
                }
            });
        } else {
            Log.e(TAG, "connection failed");
        }
    }

    private void setupGoogleApiClient() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(context)
                .enableAutoManage((FragmentActivity) context /* Activity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "GoogleApiClient.OnConnectionFailedListener has encountered a connection failure");
        EventBus.getDefault().post(new ToastMessageEvent("Connection failed"));
    }

}
