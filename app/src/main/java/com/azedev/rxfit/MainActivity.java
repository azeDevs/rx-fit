package com.azedev.rxfit;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.azedev.rxfit.databinding.ActivityMainBinding;
import com.azedev.rxfit.eventbus.LoginEvent;
import com.azedev.rxfit.eventbus.SwapFragmentEvent;
import com.azedev.rxfit.eventbus.ToastMessageEvent;
import com.azedev.rxfit.fragments.LoginFragment;
import com.azedev.rxfit.fragments.NutritionFragment;
import com.azedev.rxfit.managers.AuthSessionManager;
import com.azedev.rxfit.managers.DatabaseManager;
import com.google.firebase.database.ValueEventListener;
import com.jakewharton.rxbinding2.view.RxView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class MainActivity extends FragmentActivity {
	public static final String TAG = MainActivity.class.getName();

	private ActivityMainBinding binding;
	private ValueEventListener databaseConnectionListener;

	private AuthSessionManager authSessionManager;
	private DatabaseManager databaseManager;

	private TextView txtVersionCode;
	private ImageView imageUserAvatar;
	private ImageView firebaseStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
		databaseManager = DatabaseManager.getInstance();
		authSessionManager = new AuthSessionManager(this);
		setupViews();

		if (findViewById(R.id.fragment_container) != null) {
			if (savedInstanceState != null) return;
			EventBus.getDefault().postSticky(new SwapFragmentEvent(SwapFragmentEvent.EventDef.DEFAULT));
		}
	}

	private void setupViews() {
		firebaseStatus = binding.firebaseStatus;
		imageUserAvatar = binding.userAvatar;
		txtVersionCode = binding.textVersioncode;
		txtVersionCode.setText(getVersionNumber());
		RxView.clicks(imageUserAvatar).subscribe(o -> {
			EventBus.getDefault().post(new LoginEvent(LoginEvent.EventDef.DEFAULT));
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		authSessionManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override public void onStop() {
		databaseManager.removeDatabaseConnectionListener();
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
		super.onSaveInstanceState(outState, outPersistentState);
		// TODO: collect state from all Managers to persist between sessions
	}

	@Subscribe(threadMode = ThreadMode.BACKGROUND)
	public void onLoginEvent(LoginEvent event) {
		switch (event.eventDef) {
			case SIGN_IN:
				authSessionManager.signIn();
				break;
			case SIGN_OUT:
				authSessionManager.signOut();
				break;
		}
	}

	@Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
	public void onSwapFragment(SwapFragmentEvent event) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		switch (event.eventDef) {
			case LOGIN:
				imageUserAvatar.setImageDrawable(getDrawable(R.drawable.ic_profile_blank0));
				transaction.replace(R.id.fragment_container, new LoginFragment());
				break;
			case NUTRITION:
				new ImageLoadTask(authSessionManager.getConnectedUser().getPhotoUrl().toString(), imageUserAvatar).execute();
				transaction.replace(R.id.fragment_container, new NutritionFragment());
				break;
		}
		transaction.addToBackStack(null);
		transaction.commit();
	}

	@Subscribe(threadMode = ThreadMode.MAIN, sticky = false)
	public void onToastMessageEvent(ToastMessageEvent event) {
		Toast.makeText(this, event.message, Toast.LENGTH_SHORT).show();
	}

	private String getVersionNumber() {
		try { PackageInfo packageInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			return packageInfo.versionName;
		} catch (PackageManager.NameNotFoundException e) { e.printStackTrace(); }
		return "error";
	}

}