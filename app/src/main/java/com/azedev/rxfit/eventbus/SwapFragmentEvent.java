package com.azedev.rxfit.eventbus;

import com.google.firebase.auth.FirebaseAuth;


public class SwapFragmentEvent {

    public enum EventDef {
        DEFAULT,
        LOGIN,
        NUTRITION
    }

    public final EventDef eventDef;

    public SwapFragmentEvent(EventDef eventDef) {
        this.eventDef = getEventDef(eventDef);
    }

    private EventDef getEventDef(EventDef eventDef) {
        if (eventDef == EventDef.DEFAULT) {
            if (FirebaseAuth.getInstance().getCurrentUser() == null)
                return EventDef.LOGIN;
            else
                return EventDef.NUTRITION;
        }
        return eventDef;
    }

}
