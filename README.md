## Rx Fit

An all-purpose fitness app for Android mobile devices.
Rx Fit will allow users to enter their physical stats and inventory of foods,
which will allow Rx Fit to prescribe workout and meal plans to help them achieve their fitness goals.

#### Features

* Fitness progressValue via visual graphs over time
* Automated meal planning
* Automated workout planning

#### Libraries

* Firebase
* RxJava
* Databinding
* RecyclerView