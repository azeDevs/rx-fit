package com.azedev.rxfit.models;

public class MacroStats {

	private Double calories;
	private Double protein;
	private Double carbs;
	private Double fat;

	public MacroStats(Double calories, Double protein, Double fat, Double carbs) {
		this.calories = calories;
		this.protein = protein;
		this.carbs = carbs;
		this.fat = fat;
	}

	public MacroStats(MacroStats macroStats) {
		setMacros(macroStats);
	}

	public MacroStats() {
		voidMacros();
	}

	public MacroStats getMacros() {
		return this;
	}

	public void setMacros(MacroStats macroStats) {
		this.calories = macroStats.getCalories();
		this.protein = macroStats.getProtein();
		this.carbs = macroStats.getCarbs();
		this.fat = macroStats.getFat();
	}

	public void setCarbs(Double carbs) {
		this.carbs = carbs;
	}

	public Double getCarbs() {
		return carbs;
	}

	public void setProtein(Double protein) {
		this.protein = protein;
	}

	public Double getProtein() {
		return protein;
	}

	public void setFat(Double fat) {
		this.fat = fat;
	}

	public Double getFat() {
		return fat;
	}

	public void setCalories(Double calories) {
		this.calories = calories;
	}

	public Double getCalories() {
		return calories;
	}

	private String formatDecimals(String in, int decimalPlaces) {
		decimalPlaces = decimalPlaces == 0 || decimalPlaces < 0 ? 0 : decimalPlaces + 1;
		return in.substring(0, in.indexOf(".") + decimalPlaces);
	}

	public String getFormattedCalories() {
		return formatDecimals(calories.toString(), 0);
	}

	public String getFormattedProtein() {
		return formatDecimals(protein.toString(), 1) + " g";
	}

	public String getFormattedCarbs() {
		return formatDecimals(carbs.toString(), 1) + " g";
	}

	public String getFormattedFat() {
		return formatDecimals(fat.toString(), 1) + " g";
	}

	public void voidMacros() {
		this.calories = 0d;
		this.protein = 0d;
		this.fat = 0d;
		this.carbs = 0d;
	}

	public void addToMacros(MacroStats macroStats) {
		this.calories += macroStats.getCalories();
		this.protein += macroStats.getProtein();
		this.fat += macroStats.getFat();
		this.carbs += macroStats.getCarbs();
	}

	@Override
	public String toString() {
		return
				"Macros{" +
						"carbs = '" + carbs + '\'' +
						",protein = '" + protein + '\'' +
						",fat = '" + fat + '\'' +
						",calories = '" + calories + '\'' +
						"}";
	}

}
