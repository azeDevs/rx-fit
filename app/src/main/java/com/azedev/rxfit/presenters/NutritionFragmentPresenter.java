package com.azedev.rxfit.presenters;


import com.azedev.rxfit.fragments.NutritionFragment;


public class NutritionFragmentPresenter extends BaseFragmentPresenter<NutritionFragment> {
    public static final String TAG = NutritionFragmentPresenter.class.getName();

    public NutritionFragmentPresenter(NutritionFragment view) {
        super(view);
    }

}
