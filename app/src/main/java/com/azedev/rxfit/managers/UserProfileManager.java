package com.azedev.rxfit.managers;

import com.azedev.rxfit.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class UserProfileManager {
    public static final String TAG = UserProfileManager.class.getName();

    private static DatabaseReference userDatabase = null;

    protected UserProfileManager() {}

    public static void addUser(String userId, String name, String email) {
        userDatabase = FirebaseDatabase.getInstance().getReference();

        User user = new User(name, email);
        userDatabase.child("users").child(userId).setValue(user);
    }

}
