package com.azedev.rxfit.eventbus;

public class ToastMessageEvent {

    public final String message;

    public ToastMessageEvent(String message) {
        this.message = message;
    }

}
