package com.azedev.rxfit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.azedev.rxfit.R;
import com.azedev.rxfit.managers.DatabaseManager;
import com.azedev.rxfit.models.FoodItem;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;


public class FoodItemAdapter extends RecyclerView.Adapter<FoodItemAdapter.ViewHolder> {
    public static final String TAG = FoodItemAdapter.class.getName();


    private Context context;
    private DatabaseManager databaseManager;
    private Observer<List<FoodItem>> foodItemsSubscriber;
    private List<FoodItem> foodItemsModelList;

    // TODO: PublishSubject<View> apparently detects clicks on the viewholder maybe. Figure it out.
    // https://stackoverflow.com/questions/36497690/how-to-handle-item-clicks-for-a-recycler-view-using-rxjava
    private PublishSubject<View> viewHolderClickSubject = PublishSubject.create();

    public FoodItemAdapter(Context context) {
        this.foodItemsModelList = new ArrayList<>();
        this.context = context;
        this.databaseManager = DatabaseManager.getInstance();
        subscribeToDatabase();
    }

    private void subscribeToDatabase() {
        this.foodItemsSubscriber = new Observer<List<FoodItem>>() {
            @Override public void onSubscribe(@NonNull Disposable d) {

            }

            @Override public void onNext(@NonNull List<FoodItem> foodItems) {
                foodItemsModelList = foodItems;
            }

            @Override public void onError(@NonNull Throwable e) {

            }

            @Override public void onComplete() {
                notifyDataSetChanged();
            }
        };

        databaseManager.setFoodItemsSubscriber(foodItemsSubscriber);
    }


    @Override
    public FoodItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.listitem_food, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        RxView.clicks(view)
                .takeUntil(RxView.detaches(parent))
                .map(aVoid -> view)
                .subscribe(viewHolderClickSubject);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewName.setText(foodItemsModelList.get(position).getName());

        holder.textViewStandardServing.setText(foodItemsModelList.get(position).getUnitsOfMeasurement().getFormattedStandardValue());
        holder.textViewAbstractServing.setText(foodItemsModelList.get(position).getUnitsOfMeasurement().getFormattedAbstractValue());

        holder.textViewCalories.setText(foodItemsModelList.get(position).getFormattedCalories());
        holder.textViewProtein.setText(foodItemsModelList.get(position).getFormattedProtein());
        holder.textViewFat.setText(foodItemsModelList.get(position).getFormattedFat());
        holder.textViewCarbs.setText(foodItemsModelList.get(position).getFormattedCarbs());
    }

    @Override
    public int getItemCount(){
        return foodItemsModelList.size();
    }


    // FoodItem ViewHolder Class
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public TextView textViewStandardServing;
        public TextView textViewAbstractServing;
        public TextView textViewCalories;
        public TextView textViewProtein;
        public TextView textViewFat;
        public TextView textViewCarbs;

        public ViewHolder(View view) {
            super(view);
            textViewName = (TextView) view.findViewById(R.id.txt_name);
            textViewStandardServing = (TextView) view.findViewById(R.id.textViewStandardServing);
            textViewAbstractServing = (TextView) view.findViewById(R.id.textViewAbstractServing);
            textViewCalories = (TextView) view.findViewById(R.id.textViewCalories);
            textViewProtein = (TextView) view.findViewById(R.id.textViewProtein);
            textViewFat = (TextView) view.findViewById(R.id.textViewFat);
            textViewCarbs = (TextView) view.findViewById(R.id.textViewCarbs);
        }
    }

}
