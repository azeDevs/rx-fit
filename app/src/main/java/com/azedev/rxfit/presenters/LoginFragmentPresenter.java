package com.azedev.rxfit.presenters;

import com.azedev.rxfit.fragments.LoginFragment;


public class LoginFragmentPresenter extends BaseFragmentPresenter<LoginFragment> {
    public static final String TAG = LoginFragmentPresenter.class.getName();

    public LoginFragmentPresenter(LoginFragment view) {
        super(view);
    }

}
