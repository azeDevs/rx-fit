package com.azedev.rxfit.fragments;

import android.support.v4.app.Fragment;

import com.azedev.rxfit.presenters.BaseFragmentPresenter;


public abstract class BaseFragment<T extends BaseFragmentPresenter> extends Fragment {

    protected T presenter;

    T getFragmentPresenter() {
        return presenter;
    }

    void setFragmentPresenter(T presenter) {
        this.presenter = presenter;
    }

}
