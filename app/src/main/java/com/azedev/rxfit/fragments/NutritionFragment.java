package com.azedev.rxfit.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.azedev.rxfit.R;
import com.azedev.rxfit.adapters.FoodItemAdapter;
import com.azedev.rxfit.databinding.FragmentNutribaseBinding;
import com.azedev.rxfit.presenters.NutritionFragmentPresenter;

import io.reactivex.annotations.Nullable;


public class NutritionFragment extends BaseFragment<NutritionFragmentPresenter> {
    public static final String TAG = NutritionFragment.class.getName();

    private FragmentNutribaseBinding binding;
    private FoodItemAdapter recyclerViewAdapter;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;

    private TextView totalUnitsTextView;
    private TextView totalCaloriesTextView;
    private TextView totalProteinTextView;
    private TextView totalFatTextView;
    private TextView totalCarbsTextView;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nutribase, container, false);
        super.setFragmentPresenter(new NutritionFragmentPresenter(this));
        setupViews();
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setupViews() {
        recyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewAdapter = new FoodItemAdapter(getActivity());
        recyclerView = binding.recyclerView;
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);
        totalUnitsTextView = binding.topnav.macrostats.txtUnitCount;
        totalCaloriesTextView = binding.topnav.macrostats.txtCalorieTotal;
        totalProteinTextView = binding.topnav.macrostats.txtProteinTotal;
        totalFatTextView = binding.topnav.macrostats.txtFatTotal;
        totalCarbsTextView = binding.topnav.macrostats.txtCarbsTotal;
    }

}
