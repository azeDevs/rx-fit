package com.azedev.rxfit.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.azedev.rxfit.R;
import com.azedev.rxfit.databinding.FragmentLoginBinding;
import com.azedev.rxfit.eventbus.LoginEvent;
import com.azedev.rxfit.presenters.LoginFragmentPresenter;
import com.jakewharton.rxbinding2.view.RxView;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.annotations.Nullable;


public class LoginFragment extends BaseFragment<LoginFragmentPresenter> {
    public static final String TAG = LoginFragment.class.getName();

	private FragmentLoginBinding binding;
	private TextView txtConnecting;
	private ImageButton btnSignin;

	@Override
	void setFragmentPresenter(LoginFragmentPresenter presenter) {
		super.setFragmentPresenter(presenter);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
		super.setFragmentPresenter(new LoginFragmentPresenter(this));
		setupViews();
		return binding.getRoot();
	}

	private void setupViews() {
		txtConnecting = binding.textConnecting;
		btnSignin = binding.buttonSignin;

		RxView.clicks(btnSignin).subscribe(o -> {
			EventBus.getDefault().post(new LoginEvent(LoginEvent.EventDef.DEFAULT));
		});
	}

}
