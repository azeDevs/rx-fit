package com.azedev.rxfit.presenters;


import com.azedev.rxfit.fragments.BaseFragment;


public abstract class BaseFragmentPresenter<T extends BaseFragment> {
    public static final String TAG = BaseFragmentPresenter.class.getName();

    protected T view;

    public BaseFragmentPresenter(T view) {
        this.view = view;
    }

    void setView(T view) {
        this.view = view;
    }

    public T getView() {
        return view;
    }

}
